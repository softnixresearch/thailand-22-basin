# Thailand 22 Basin
- ข้อมูลต้นฉบับจากสำนักงานทรัพยากรน้ำแห่งชาติ http://www.onwr.go.th/?page_id=9893
- ทำการเพิ่ม Column ตำบล อำเภอ และจังหวัด เพื่อความสะดวกในการนำไปใช้งาน
- จัดแบ่งแผนที่ 22 ลุ่มน้ำตามเขตตำบลจัดทำในรูปแบบ ​SHP file

# ตัวอย่างข้อมูลแบ่งตามคอลัมน์

![](./images/22basinex.png)

# ตัวอย่างแผนที่ 22 ลุ่มน้ำจัดแบ่งตามตำบล

![](./images/22BasinTHMapEx.png)

# ผู้พัฒนา
- Data Team (data@softnix.co.th)
- บอส,ตุ้ม,บิ้ง,ออม
